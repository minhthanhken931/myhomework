'''Bài tập về nhà: Hãy sử dụng câu lệnh cmd để xóa my go ram ra khỏi u ni ve. Sau đó tạo 1 file gồ rem . py. Và hãy viết 1 chương trình python để hiện thì cm, với các yêu cầu sau: 

s = "Today is nice day, so we can go anywhere"
s1 = "Today is nice day"
s2 = "So we can go anywhere"

a) Hãy đếm số ký tự trong chuỗi đã cho 
b) Hãy xuất vị trí -5 đến -9 
c) Xuất tất cả các ký tự trong chuỗi với bước nhảy là 3 
d) Hãy tách chuỗi đã cho thành chuỗi mới: 
s1 = "Today is nice day" 
s2 = "So we can go anywhere" 
e) Hãy hợp nhất chuỗi s1 với s2 lại. Và sắp xếp
sắp xếp theo chữ cái A->Z
f) chuyển các chuỗi sau khi hợp nhất thành chuỗi ký tự in hoa '''


s="Today is nice day , so we can go anywhere"
s1=" Today is nice day"
s2=" So we can go anuwhere"

#a
print(len(s))          # sử dụng hàm lent() chuyên dụng đếm kí tự 
print(len(s1))
print(len(s2))
#b
s[-9:-5]          # vị trí index đếm từ bé đến lớn (đếm từ lớn đến bé sẽ không ra kết quả )
s1[-9:-5]           # bắt đầu từ -9 và kết thúc -5 
s2[-9:-5]
#c
s[0::3]            # bắt đầu từ 0 đến hết chuỗi với bước nhảy là 3 tính cả khoảng trắng
s1[0::3]
s2[0::3]
#d
print(s1.split())
print(s2.split())
#e
'''add = s1.join(s2)''' # từ phần tử của s2 sẽ được thêm vào s1 vào cuối string và lặp lại liên tục cho đến khi s2 được thêm hết 
add = s1+s2 
print(sorted(add))   # sắp xếp theo thứ tự từ a-z theo giá trị của bảng ASCII có cả dấu 
#f 
print(add.upper())   # sử dụng hàm upper() để chuyển đổi tất cả thành chữ in hoa và lower() sẽ chuyển về chữ thường
